import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AnyListPageModule} from '@app/any-list-page/any-list-page.module';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home-page',
    pathMatch: 'full'
  },
  {
    path: 'home-page',
    loadChildren: './home-page/home-page.module#HomePageModule'
  },
  {
    path: 'catalog-page',
    loadChildren: './catalog-page/catalog-page.module#CatalogPageModule'
  },
  {
    path: 'product-card/:id',
    loadChildren: './product-card/product-card.module#ProductCardModule'
  },
  {
    path: 'basket-page',
    loadChildren: './basket-page/basket-page.module#BasketPageModule'
  },
  {
    path: 'any-list-page/:type',
    loadChildren: './any-list-page/any-list-page.module#AnyListPageModule'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
