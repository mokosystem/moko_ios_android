import {Injectable} from '@angular/core';
import {NativeStorage} from '@ionic-native/native-storage/ngx';
import {AlertController, LoadingController} from '@ionic/angular';
import {resolve} from 'q';
import {Observable} from 'rxjs/internal/Observable';
import {environment} from '@env/environment';
import {HttpClient} from '@angular/common/http';
import {AuthService} from '@app/shared-modules/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  uniqueDeviceId: string;
  basketProductQuantity: any;
  logs: string;
  shopInfo: any;
  appInfo: any;

  constructor(private nativeStorage: NativeStorage,
              public alertController: AlertController,
              private loadingController: LoadingController,
              private http: HttpClient,
              private authService: AuthService) {
  }

  getApp(): Observable<any> {
    return this.http
        .get<any>(environment.baseUri + environment.catalogApp, {headers: this.authService.getAuthHeaders()});
  }

  getShop(): Observable<any> {
    return this.http
        .get<any>(environment.baseUri + environment.catalogShop, {headers: this.authService.getAuthHeaders()});
  }

  addToBasket(itemToBasket: any) {
    const newStorage = [];

    this.getCachedStorage()
        .then((data: any[]) => {
              this.addProductLogic(itemToBasket, newStorage, data);

              return this.nativeStorage
                  .setItem('storage', {storage: newStorage})
                  .then(() => this.setBasketProductQuantity(newStorage));
            },
            () => {
              return this.nativeStorage
                  .setItem('storage', {storage: [itemToBasket]})
                  .then(() => this.setBasketProductQuantity([itemToBasket]));
            })
        .then();
  }

  setBasketProductQuantity(data: any[]): any[] {
    this.basketProductQuantity = data.length;
    return this.basketProductQuantity;
  }

  clearStorage() {
    return this.nativeStorage.clear()
        .then((data) => {
          this.setBasketProductQuantity([]);
          return resolve(data);
        });
  }

  getCachedStorage() {
    return this.nativeStorage.getItem('storage')
        .then((storage: any) => resolve(storage.storage ? storage.storage : []));
  }

  addProductLogic(itemToBasket, newStorage, cachedStorage: any[]) {
    if (cachedStorage.length) {
      newStorage.push(...cachedStorage);

      const product = cachedStorage.find(item => item._id === itemToBasket._id);
      if (!product) { newStorage.push(itemToBasket); }

    } else {
      newStorage.push(itemToBasket);
    }
  }

  async console(message: any) {
    const alert = await this.alertController.create({
      header: 'Alert',
      subHeader: 'Log',
      message: typeof message === 'object' ? JSON.stringify(message) : message,
      buttons: ['OK']
    });

    await alert.present();
  }

  async error(message: any) {
    const alert = await this.alertController.create({
      header: 'Error!',
      message: message,
      buttons: ['Close']
    });

    await alert.present();
  }

  addLog(log) {
    this.logs = this.logs + `<br>${log}`;
  }

  async showLog() {
    const alert = await this.alertController.create({
      header: 'App Logs',
      message: this.logs,
      buttons: ['Зыкрыть']
    });
    await alert.present();
  }

  async showLoading() {
    const loading = await this.loadingController.create({
      duration: 1000
    });
    return await loading.present();
  }

  async stopLoading() {
    try {
      return await this.loadingController.dismiss();
    } catch (e) {
    }
  }
}
