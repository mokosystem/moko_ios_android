import { NgModule } from '@angular/core';
import { BasketPageComponent } from './basket-page.component';
import {AppPageHeaderModule} from '../shared-modules/app-page-header/app-page-header.module';
import {AppCommonModules} from '../shared-modules/common-modules.module';
import {RouterModule} from '@angular/router';
import {AppListPageModule} from '../shared-modules/app-list-page/app-list-page.module';
import { LeaveRequestComponent } from './leave-request/leave-request.component';

@NgModule({
  declarations: [BasketPageComponent, LeaveRequestComponent],
  imports: [
    AppCommonModules,
    AppListPageModule,
    AppPageHeaderModule,
    RouterModule.forChild([
      {
        path: '',
        component: BasketPageComponent
      }
    ])
  ]
})
export class BasketPageModule { }
