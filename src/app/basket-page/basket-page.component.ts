import {Component, OnInit} from '@angular/core';
import {BasketPageService} from './basket-page.service';
import {NativeStorage} from '@ionic-native/native-storage/ngx';
import {AppService} from '../app.service';
import {AlertController, Platform} from '@ionic/angular';
import {CatalogPageService} from '../catalog-page/catalog-page.service';
import {combineLatest} from 'rxjs/internal/observable/combineLatest';
import {Product} from '../shared-models/product';
import {Router} from '@angular/router';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-basket',
  templateUrl: './basket-page.component.html',
  styleUrls: ['./basket-page.component.scss']
})
export class BasketPageComponent implements OnInit {
  deviceStorage;
  response;
  products: Product[];
  totalCost = 0;
  loader = false;

  constructor(private basketService: BasketPageService,
              private nativeStorage: NativeStorage,
              private appService: AppService,
              private platform: Platform,
              private listPageService: CatalogPageService,
              private router: Router,
              public alertController: AlertController) {
  }

  ngOnInit() {
    this.platform.ready().then(() => {
      this.getDeviceStorage();
    });
  }

  getDeviceStorage() {
    this.loader = true;
    this.appService.getCachedStorage()
        .then(
            y => {
              this.deviceStorage = y;
              const getItems = this.deviceStorage.map(item => this.listPageService.getProductById(item._id)
                  .pipe(
                      map(x => {
                        x.quantity = item.quantity;
                        x.price = +x.price;
                        this.totalCost += x.price;
                        return x;
                      })
                  )
              );
              combineLatest(...getItems).subscribe((data: Product[]) => {
                this.products = [...data];
                this.loader = false;
              });
            },
            error => {
              this.products = [];
              this.loader = false;
            }
        );
  }

  async clearStorage() {
    const alert = await this.alertController.create({
      header: 'Удаление',
      message: 'Удалить все продукты из корзины',
      buttons: [
        {
          text: 'Нет',
          role: 'cancel',
          cssClass: 'secondary'
        }, {
          text: 'Да',
          handler: () => {
            this.appService.clearStorage()
                .then(() => this.getDeviceStorage());
          }
        }
      ]
    });
    await alert.present();
  }

  async leaveRequest() {
    const alert = await this.alertController.create({
      header: 'Ваши контактные данные',
      inputs: [
        {
          name: 'email',
          type: 'text',
          placeholder: 'Email*'
        },
        {
          name: 'phone',
          type: 'text',
          placeholder: 'Телефон*'
        },
        {
          name: 'customer',
          type: 'text',
          placeholder: 'Имя*'
        },
        {
          name: 'note',
          type: 'text',
          placeholder: 'Заметка (не обязательно)'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary'
        }, {
          text: 'Ok',
          handler: (data) => {
            this.makeOrder(data);
          }
        }
      ]
    });

    await alert.present();
  }

  async alertContactInfo() {
    const alert = await this.alertController.create({
      header: 'Не все поля заполнены',
      message: 'Заполните поля, помеченные <strong>*</strong>',
      buttons: [{text: 'Ок'}]
    });

    await alert.present();
  }


  makeOrder(order) {
    if (order.customer && order.phone && order.email) {
      order.deviceid = this.appService.uniqueDeviceId ? this.appService.uniqueDeviceId : 'not a device';
      order.orderitems = this.deviceStorage;
      order.date = '';

      this.basketService.postNewOrder(order).subscribe(data => {
        this.response = data;
      });
    } else {
      this.alertContactInfo();
    }

  }

  selectItem(item) {
    this.listPageService.selectedItem = item;
    this.router.navigate([`product-card/${item._id}`]);
  }

  addItem(item) {
    item.quantity++;
  }

  deleteItem(item) {
    if (item.quantity > 0) {
      item.quantity--;
    }
  }

}
