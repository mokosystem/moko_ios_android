import {Injectable} from '@angular/core';
import {Order} from '../shared-models/order';
import {HttpClient} from '@angular/common/http';
import {environment} from '@env/environment';
import {AuthService} from '@app/shared-modules/auth.service';

@Injectable({
  providedIn: 'root'
})
export class BasketPageService {

  constructor(private http: HttpClient, private auth: AuthService) {
  }

  postNewOrder(form: Order) {
    const url = `${environment.baseUri}${environment.catalogOrders}`;
    return this.http
        .post(url, form, {headers: this.auth.getAuthHeaders()});
  }
}
