import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AppService} from '@app/app.service';
import {AnyListPageService} from '@app/any-list-page/any-list-page.service';

@Component({
  selector: 'app-any-list-page',
  templateUrl: './any-list-page.component.html',
  styleUrls: ['./any-list-page.component.scss']
})
export class AnyListPageComponent implements OnInit {
  typeOfList: string;
  list: any[];

  constructor(private activatedRoute: ActivatedRoute,
              private anyListService: AnyListPageService) { }

  ngOnInit() {
    this.typeOfList = this.activatedRoute.snapshot.paramMap.get('type');
    this.anyListService.getList(this.typeOfList).subscribe(data => this.list = data);
  }

  selectItem(item) {

  }

}
