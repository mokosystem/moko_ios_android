import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {AuthService} from '@app/shared-modules/auth.service';
import {Observable} from 'rxjs/internal/Observable';
import {environment} from '@env/environment';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AnyListPageService {

  constructor(private http: HttpClient, private authService: AuthService) { }

  getList(typeOfList): Observable<any> {
    const params = new HttpParams().set('category', typeOfList);
    return this.http
        .get<any>(environment.baseUri + environment.catalogMessages,
            {
              headers: this.authService.getAuthHeaders(),
              params: params
            })
        .pipe(
            map((list: any) => {
              for (let i = 0; i < list.length; i++) {
                if (list[i].images) {
                  for (let j = 0; j < list[i].images.length; j++) {
                    list[i].images[j] = this.getImage(list[i].images[j].id);
                  }
                }
              }
              return list;
            })
        );
  }

  getImage(id): Observable<any> {
    const url = `${environment.baseUri}${environment.catalogImage}/${id}`;
    return this.http.get(
        url, {headers: this.authService.getAuthHeaders()})
        .pipe(
            map(messageBody => messageBody['image'])
        );
  }
}
