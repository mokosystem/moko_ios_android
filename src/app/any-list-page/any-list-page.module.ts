import { NgModule } from '@angular/core';
import { AnyListPageComponent } from './any-list-page.component';
import {AppCommonModules} from '@app/shared-modules/common-modules.module';
import {AppListPageModule} from '@app/shared-modules/app-list-page/app-list-page.module';
import {AppPageHeaderModule} from '@app/shared-modules/app-page-header/app-page-header.module';
import {RouterModule} from '@angular/router';

@NgModule({
  declarations: [AnyListPageComponent],
  imports: [
    AppCommonModules,
    AppListPageModule,
    AppPageHeaderModule,
    RouterModule.forChild([
      {
        path: '',
        component: AnyListPageComponent
      }
    ])
  ]
})
export class AnyListPageModule { }
