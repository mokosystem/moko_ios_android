import { Injectable } from '@angular/core';
import {HttpHeaders} from '@angular/common/http';
import {FormGroup} from '@angular/forms';
import {Observable} from 'rxjs/internal/Observable';
import {of} from 'rxjs/internal/observable/of';
import {environment} from '@env/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  getAuthHeaders(form?: FormGroup): HttpHeaders {
    let user;
    if (form) {
      user = {login: form.value['login'], password: form.value['password']};
    } else {
      user = {login: environment.login, password: environment.password};
    }
    let headers = new HttpHeaders();
    headers = headers.append('Authorization', 'Basic ' + btoa(user.login + ':' + user.password));
    // headers = headers.append("Content-Type", "application/x-www-form-urlencoded");
    return headers;
  }
}

