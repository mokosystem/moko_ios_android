/*
  Универсальный, обший модуль для импорта во все модули
 */

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    IonicModule,
    FormsModule
  ],
  exports: [
    IonicModule,
    CommonModule,
    FormsModule
  ]
})
export class AppCommonModules {
}
