import {HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/internal/Observable';
import {AppService} from '../app.service';
import {Injectable} from '@angular/core';
import {catchError, map, switchMap} from 'rxjs/operators';
import {of} from 'rxjs/internal/observable/of';

@Injectable()
export class InterceptHttpService implements HttpInterceptor {
  apiUrl = 'moko.by';

  constructor(
      private appService: AppService
  ) {
  }

  intercept(
      req: HttpRequest<any>,
      next: HttpHandler
  ): Observable<any> {
    if (req.url.includes(this.apiUrl)) {
      this.appService.showLoading().then();
    }

    return next.handle(req).pipe(
        map((event) => {
          this.appService.stopLoading().then();
          return event;
        }),
        catchError((error) => {
          this.appService.stopLoading();
          return of(error);
        })
    );
  }
}
