import {NgModule} from '@angular/core';
import {AppListPageComponent} from './app-list-page.component';
import {AppPageHeaderModule} from '../app-page-header/app-page-header.module';
import {AppCommonModules} from '../common-modules.module';

@NgModule({
  declarations: [AppListPageComponent],
  imports: [
    AppCommonModules,
    AppPageHeaderModule
  ],
  exports: [
    AppListPageComponent
  ]
})
export class AppListPageModule {
}
