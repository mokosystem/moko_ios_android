import {ErrorHandler, Injectable} from '@angular/core';
import {AlertController} from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ErrorsHandler implements ErrorHandler {
  constructor(public alertController: AlertController) {}

  async handleError(error: Error) {
    // Do whatever you like with the error (send it to the server?)
    // And log it to the console
    console.error('It happens: ', error);

    const alert = await this.alertController.create({
      header: 'Alert',
      subHeader: 'Subtitle',
      message: error.stack,
      buttons: ['OK']
    });

    await alert.present();
  }
}