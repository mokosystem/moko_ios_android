import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Router} from '@angular/router';
import {AppService} from '../../app.service';

@Component({
  selector: 'app-page-header',
  templateUrl: './app-page-header.component.html',
  styleUrls: ['./app-page-header.component.scss']
})
export class AppPageHeaderComponent implements OnInit {
  @Input() title: string;
  @Input() backButtonEnabled = false;
  @Input() loader = true;
  @Input() clearButtonEnabled = false;
  @Input() clearStorageFunction;
  @Output() clearStorage = new EventEmitter();
  constructor(private router: Router,
              public appService: AppService) {
  }

  ngOnInit() {
  }

  navigateToBasket() {
    this.router.navigate(['basket-page']);
  }

}
