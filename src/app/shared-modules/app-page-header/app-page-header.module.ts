import {NgModule} from '@angular/core';
import {AppPageHeaderComponent} from './app-page-header.component';
import {AppCommonModules} from '../common-modules.module';

@NgModule({
  declarations: [AppPageHeaderComponent],
  imports: [
    AppCommonModules
  ],
  exports: [
    AppPageHeaderComponent
  ]
})
export class AppPageHeaderModule {
}
