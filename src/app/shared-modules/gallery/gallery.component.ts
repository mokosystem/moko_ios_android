import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {

  @Input() images;
  @Input() modalController;

  slideOpts = {
    effect: 'flip'
  };

  constructor() { }

  ngOnInit() {
  }

  dismiss() {
    this.modalController.dismiss();
  }

}
