import { NgModule } from '@angular/core';
import { GalleryComponent } from './gallery.component';
import {AppCommonModules} from '../common-modules.module';

@NgModule({
  declarations: [GalleryComponent],
  imports: [
    AppCommonModules
  ],
  exports: [
    GalleryComponent
  ],
  entryComponents: [
    GalleryComponent
  ]
})
export class GalleryModule { }
