import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {HomePageComponent} from './home-page.component';
import {AppCommonModules} from '../shared-modules/common-modules.module';
import {AppListPageModule} from '../shared-modules/app-list-page/app-list-page.module';
import {AppPageHeaderModule} from '../shared-modules/app-page-header/app-page-header.module';

@NgModule({
  declarations: [HomePageComponent],
  imports: [
        AppCommonModules,
        AppListPageModule,
        AppPageHeaderModule,
        RouterModule.forChild([
            {
                path: '',
                component: HomePageComponent
            }
        ])
    ]
})
export class HomePageModule {
}
