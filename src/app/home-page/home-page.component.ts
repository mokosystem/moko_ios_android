import {Component, OnInit} from '@angular/core';
import {HomePageService} from '@app/home-page/home-page.service';


@Component({
  selector: 'app-home-page',
  templateUrl: 'home-page.component.html',
  styleUrls: ['home-page.component.scss'],
})
export class HomePageComponent implements OnInit {
  data;
  constructor(private homePageService: HomePageService) {
  }

  ngOnInit(): void {
  }
}
