import { NgModule } from '@angular/core';
import { ProductCardComponent } from './product-card.component';
import {AppCommonModules} from '../shared-modules/common-modules.module';
import {AppPageHeaderModule} from '../shared-modules/app-page-header/app-page-header.module';
import {AppListPageModule} from '../shared-modules/app-list-page/app-list-page.module';
import {RouterModule} from '@angular/router';
import {GalleryModule} from '../shared-modules/gallery/gallery.module';

@NgModule({
  declarations: [ProductCardComponent],
  imports: [
    AppCommonModules,
    AppListPageModule,
    AppPageHeaderModule,
    GalleryModule,
    RouterModule.forChild([
      {
        path: '',
        component: ProductCardComponent
      }
    ])
  ]
})
export class ProductCardModule { }
