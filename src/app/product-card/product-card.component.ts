import {Component, OnInit, ViewChild} from '@angular/core';
import {CatalogPageService} from '../catalog-page/catalog-page.service';
import {Product} from '../shared-models/product';
import {ActivatedRoute} from '@angular/router';
import {AppService} from '../app.service';
import {ImagePlaceholder} from '../shared-models/image-ph';
import {of} from 'rxjs/internal/observable/of';
import {AlertController, ModalController} from '@ionic/angular';
import {GalleryComponent} from '../shared-modules/gallery/gallery.component';

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.scss']
})
export class ProductCardComponent implements OnInit {
  @ViewChild('tabs') tabs;
  selectedItem: Product;
  slideOpts = {
    effect: 'flip'
  };

  constructor(private catalogPageService: CatalogPageService,
              private activatedRoute: ActivatedRoute,
              private appService: AppService,
              public modalController: ModalController,
              public alertController: AlertController) {
  }

  ngOnInit() {
    const item = this.catalogPageService.selectedItem;
    if (item) {
      this.selectedItem = item;
    } else {
      const id = this.activatedRoute.snapshot.paramMap.get('id');
      this.catalogPageService.getProductById(id).subscribe(data => this.selectedItem = data);
    }
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: GalleryComponent,
      componentProps: {
        modalController: this.modalController,
        images: this.selectedItem.images
      },
      showBackdrop: true
    });
    return await modal.present();
  }

  async addToBasket(item: Product) {
    const alert = await this.alertController.create({
      header: item.title,
      message: 'Добавить продукт в корзину',
      buttons: [
        {
          text: 'Нет',
          role: 'cancel',
          cssClass: 'secondary'
        }, {
          text: 'Да',
          handler: (blah) => {
            this.appService.addToBasket({
              _id: item._id,
              quantity: 1,
              cost: item.price
            });
          }
        }
      ]
    });

    await alert.present();
  }
}
