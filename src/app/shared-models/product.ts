import {Image} from './image';
import {Observable} from 'rxjs/internal/Observable';

export interface Product {
  _id: string;
  category: string;
  description: string;
  images?: Image[] | Observable<any>[];
  links: ProductLink[];
  price: any;
  product_options: ProductOption[];
  store: string;
  title: string;
}

export interface ProductLink {
  link_type: string;
  name: string;
  url: string;
}

export interface ProductOption {
  name: string;
  value: string;
}
