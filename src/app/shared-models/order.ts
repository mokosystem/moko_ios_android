export interface Order {
  note: string;
  customer: string;
  phone: string;
  email: string;
  date: any;
  deviceid: string;
  orderitems: OrderItems[];
}

export interface OrderItems {
  _id: string;
  quantity: number;
  price: number;
}
