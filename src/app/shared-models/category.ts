import {Image} from './image';

export interface Category {
  '_id': string;
  'catalog': string;
  'category': string;
  'description': string;
  'images': Image[];
  'store': string;
}
