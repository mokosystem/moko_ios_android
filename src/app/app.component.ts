import {Component} from '@angular/core';
import {Platform} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {UniqueDeviceID} from '@ionic-native/unique-device-id/ngx';
import {AppService} from './app.service';
import {NativeStorage} from '@ionic-native/native-storage/ngx';
import {map} from 'rxjs/operators';
import {combineLatest} from 'rxjs/internal/observable/combineLatest';
import {Observable} from 'rxjs/internal/Observable';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  loaded = false;
  public appPages = [
    {
      title: 'Home',
      url: '/home-page',
      icon: 'home'
    },
    {
      title: 'Catalog',
      url: '/catalog-page',
      icon: 'list'
    },
    {
      title: 'Basket',
      url: '/basket-page',
      icon: 'basket'
    },
    {
      title: 'Any List Page',
      url: '/any-list-page/Новости',
      icon: 'list'
    },
  ];

  constructor(
      private platform: Platform,
      private splashScreen: SplashScreen,
      private statusBar: StatusBar,
      private uniqueDeviceID: UniqueDeviceID,
      private nativeStorage: NativeStorage,
      public appService: AppService,
  ) {
    this.initializeApp();
  }

  getInfo(): Observable<any> {
    return combineLatest(
        this.appService.getShop().pipe(map(data => data[0])),
        this.appService.getApp().pipe(map(data => data[0])));
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.getInfo().subscribe(([shop, app]) => {
        this.appService.shopInfo = shop;
        this.appService.shopInfo = app;

        this.appService.console(this.appService.shopInfo)
        this.appService.console(this.appService.appInfo)

        this.loaded = true;

        this.statusBar.styleDefault();
        this.splashScreen.hide();

        this.getDeviceUniqueId();
        this.getDeviceStorage();
      });

    });
  }

  getDeviceUniqueId() {
    this.uniqueDeviceID.get()
        .then((uuid: string) => this.appService.uniqueDeviceId = uuid)
        .catch((error: any) => console.log(error));
  }

  getDeviceStorage() {
    this.nativeStorage.keys().then(keys => {
      if (keys.indexOf('storage') !== -1) {
        this.appService.getCachedStorage()
            .then(data => {
              this.appService.setBasketProductQuantity(data);
            });
      } else {
        this.appService.setBasketProductQuantity([]);
      }
    });
  }

  showLog() {
    this.appService.showLog();
  }

}
