import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CatalogPage } from './catalog-page.component';
import {AppCommonModules} from '../shared-modules/common-modules.module';
import {AppPageHeaderModule} from '../shared-modules/app-page-header/app-page-header.module';
import {AppListPageModule} from '../shared-modules/app-list-page/app-list-page.module';

@NgModule({
  declarations: [CatalogPage],
  imports: [
    AppCommonModules,
    AppListPageModule,
    AppPageHeaderModule,
    RouterModule.forChild([
      {
        path: '',
        component: CatalogPage
      },
      {
        path: '/:type',
        component: CatalogPage
      }
    ])
  ]
})
export class CatalogPageModule {}
