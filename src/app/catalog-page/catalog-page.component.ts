import {Component, OnInit} from '@angular/core';
import {CatalogPageService} from './catalog-page.service';
import {Category} from '../shared-models/category';
import {Product} from '../shared-models/product';
import {Router} from '@angular/router';

@Component({
  selector: 'app-catalog',
  templateUrl: 'catalog-page.component.html',
  styleUrls: ['catalog-page.component.scss']
})
export class CatalogPage implements OnInit {
  categories: Category[];
  products: Product[];
  selectedCategory = '';

  constructor(private catalogPageService: CatalogPageService, private router: Router) {
  }

  ngOnInit(): void {
    this.catalogPageService.getCategories().subscribe((data: any[]) => {
      this.categories = data;
    });
    this.getProducts();
  }

  getProducts(category?: string) {
    this.catalogPageService.getProducts(category).subscribe((data: any[]) => {
      this.products = data;
    });
  }

  selectChange(value: any) {
    this.getProducts(value.detail.value);
  }

  selectItem(item) {
    this.catalogPageService.selectedItem = item;
    if (this.catalogPageService.selectedItem) {
      this.router.navigate([`product-card/${item._id}`]);

    }
  }

}
