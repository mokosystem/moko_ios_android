import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/internal/Observable';
import {HttpClient, HttpParams} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {Category} from '../shared-models/category';
import {Product} from '../shared-models/product';
import {environment} from '@env/environment';
import {AuthService} from '@app/shared-modules/auth.service';

@Injectable({
  providedIn: 'root'
})
export class CatalogPageService {
  selectedItem: Product;

  constructor(private http: HttpClient, private authService: AuthService) {
  }

  getCategories(): Observable<Category[]> {
    return this.http
        .get<any>(environment.baseUri + environment.catalogCategory, {headers: this.authService.getAuthHeaders()});
  }

  getProducts(category?, page?, pageSize?, filter?): Observable<Product[]> {
    let params = new HttpParams();

    if (category) {
      params = params.set('category', category);
    }
    if (filter) {
      params = params.set('filter', `${filter.key}:${filter.value}`);
    }

    if (pageSize) {
      const offset = page * pageSize;
      params = params.set('offset', offset.toString());
      params = params.set('perPage', pageSize.toString());
    }

    return this.http
        .get(environment.baseUri + environment.catalogProducts, {headers: this.authService.getAuthHeaders(), params: params})
        .pipe(
            map(items => {
              const item = <any>items;
              for (let i = 0; i < item.length; i++) {
                if (item[i].images) {
                  for (let j = 0; j < item[i].images.length; j++) {
                    item[i].images[j] = this.getImage(item[i].images[j].id);
                  }
                }
              }
              return item;
            })
        );
  }

  getProductById(id): Observable<any> {
    const url = `${environment.baseUri}${environment.catalogProducts}/${id}`;
    return this.http.get(url, {headers: this.authService.getAuthHeaders()})
        .pipe(
            map(items => {
              const item = <any>items;
              for (let i = 0; i < item.length; i++) {
                if (item[i].images) {
                  for (let j = 0; j < item[i].images.length; j++) {
                    item[i].images[j] = this.getImage(item[i].images[j].id);
                  }
                }
              }
              return item;
            }),
            map(messageBody => messageBody[0])
        );
  }

  getImage(id): Observable<any> {
    const url = `${environment.baseUri}${environment.catalogImage}/${id}`;
    return this.http.get(
        url, {headers: this.authService.getAuthHeaders()})
        .pipe(
            map(messageBody => messageBody['image'])
        );
  }


}
